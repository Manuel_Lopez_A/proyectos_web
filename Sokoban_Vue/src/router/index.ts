import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'
// También se puede importar el componente como en la linea de abajo y "en const routes: Array" hacer component: sokobanMain
// como muestra el texto comentado
/* import sokobanMain from '../components/sokobanMain.vue' */

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "about" */ '../components/sokobanMain.vue')
    /* component: sokobanMain */
  },
  {
    path: '/helpPage',
    name: 'helpPage',

    component: () => import(/* webpackChunkName: "about" */ '../components/helpPage.vue')
  },
  {
    path: '/playGame',
    name: 'playGame',

    component: () => import(/* webpackChunkName: "about" */ '../components/playGame.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
