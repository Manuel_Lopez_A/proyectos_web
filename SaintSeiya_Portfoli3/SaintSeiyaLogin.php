<?php


session_start();



//traemos los valores que hemos capturado con AJAX:
$mail = $_POST["usuariomail"];
$pw = $_POST["usuariopw"];
$logingOut = $_POST["salir"];

//preparamos valores de la conexión
$servername = "localhost";
$username = "root";
$password = "super3";
$dbname = "webanimes";
$table ="users";



try {
    if($_SESSION['login_IN'] == null){
        // Con esto hace la conexión
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // Set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



        //preparamos sentencia SQL para traer todos los nombres y passwords.
        $sql = $conn->prepare("SELECT nom, password FROM $table");
        // ejecutamos la sentencia SQL
        $sql->execute();
        //metemos el resultado en un array:
        $listaUsers = $sql->fetchAll(PDO::FETCH_ASSOC);


        //creamos dos variables nuevas para control usuario y password:
        $us_ok = false;
        $pw_ok = false;


        //ahora buscamos en la lista con un bucle foreach:
        for($i = 0; $i < count($listaUsers); $i++){
            if($listaUsers[$i]["nom"]== $mail){
                $us_ok = true;
                if($listaUsers[$i]["password"] == $pw) $pw_ok = true;
            }
        }

        // si user TRUE:
        if($us_ok){
            // user TRUE + pw TRUE
            if($pw_ok){

// <!-- ***********************************************************************************************-->

                $_SESSION['login_IN'] = $mail;

                print_r(json_encode([
                    "estat" => "OK",
                    "error" => "",
                    "usuari" => $mail
                ]));

            }
            // user TRUE + pw FALSE
            else print_r(json_encode([
                "estat" => "KO",
                "error" => "Contraseña incorrecta",
                "usuari" => $mail
            ]));
        }
        // user FALSE
        else{
            print_r(json_encode([
                "estat" => "KO",
                "error" => "El usuario no existe",
                "usuari" => $mail
            ]));

        }


    }
    else {
        if($logingOut){
            $_SESSION['login_IN'] = null;
            print_r(json_encode("SALIENDO"));
        } else {
            print_r(json_encode([
                "estat" => "KO",
                "error" => "SESSION ABIERTA",
                "usuari" => $_SESSION['login_IN']." CIERRA SESIÓN"
            ]));
        }

    }


}  catch(PDOException $e) {
    // Devolvemos KO y el error que ha producido
    echo(json_encode([
        "estat" => "KO",
        "error" => $e,
        "usuari" => "PELELE"
    ]));
}

$conn = null;



