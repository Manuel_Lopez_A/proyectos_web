$("#btnLog").on('click', function (){

    // AQUÍ T0DO ES COMO ANTES, COJO LOS VALORES Y LOS ENVÍO A PHP
    event.preventDefault();
    $.ajax({

        method: "post",
        url:"SaintSeiyaLogin.php",
        data:{"usuariomail": $("#emailInput").val(), "usuariopw": $("#pwInput").val()},
        dataType: 'json',

        // Y AQUÍ ES CUANDO PHP ME DEVUELVE LA DATA Y TENGO QUE HACER ALGO CON ELLA....
        success: function (data) {
            if (data["estat"] == "KO"){
                $("#respuesta").css("color", "red");
                console.log(data);
                $("#respuesta").text("ERROR: "+data["error"]+" --> "+ data["usuari"]);
            } else {
                $("#respuesta").css("color", "green");
                console.log(data);
                $("#respuesta").text("OK " + data["usuari"] + " Login CORRECTO");
            }

        },
        error: function (){

            console.log("mal");
            alert("error login");
        }
    })
});

$("#btnLogout").on('click', function (){
    event.preventDefault();
    $.ajax({

        method: "post",
        url:"SaintSeiyaLogin.php",
        data:{"salir": true },
        dataType: 'json',
        success: function (data) {
            $("#respuesta").css("color", "green");
            $("#respuesta").text("LOGOUT");
        },
        error: function (){

            console.log("mal");
            alert("error logout");
        }
    })
});

