$("#animeAdd").on('click', function (){

    // AQUÍ T0DO ES COMO ANTES, COJO LOS VALORES Y LOS ENVÍO A PHP
    event.preventDefault();
    $.ajax({

        method: "post",
        url:"SaintSeiyaAnhadir.php",
        data:{"animeNombre": $("#animeInput").val()},
        dataType: 'json',

        // Y AQUÍ ES tengo que crear la tabla con el JSON que me retorna
        success: function (data) {

            if (data["estat"]== "KO"){
                $("#respuesta").css("color", "red");
                console.log(data);
                $("#respuesta").text("ERROR: "+ data["anime"]+"<----" +data["error"]);
            } else {
                $("#respuesta").css("color", "green");
                console.log(data);
                $("#respuesta").text("OK, anime: <<" + data["anime"] + ">> insertado");
            }
        },
        error: function (){

            console.log("mal");
            alert("A que no sabes dónde está el error?");
        }
    })
});



$("#votacionAdd").on('click', function(){

    event.preventDefault();
    $.ajax({

        method: "post",
        url:"SaintSeiyaAnhadirVotacion.php",
        data:{"animeNombre": $("#animeInput").val()},
        dataType: 'json',

        success: function (data) {

            if (data["estat"]== "KO"){
                $("#respuesta").css("color", "red");
                console.log(data);
                $("#respuesta").html("YA HAY VOTACIONES: <br>"+"- Usuario: "+data["user"]+"<br>- Anime: "+data["anime"]);
            } else if (data["estat"]== "OK"){
                $("#respuesta").css("color", "green");
                console.log(data);
                $("#respuesta").html("VOTACIÓN AÑADIDA: <br>"+"- Usuario: "+data["user"]+"<br>- Anime: "+data["anime"]+"<br>- Votación: "+data["votacion"]);
            } else {
                $("#respuesta").css("color", "green");
                $("#respuesta").html("ACTUALIZACIÓN VOTACIONES<br> - Usuario: "+data["user"]+"<br>- Anime: "+data["anime"]+"<br>- Votación: "+data["votacion"]);
                console.log(data);

            }

        },
        error: function (){

            console.log("mal");
            alert("No se han transferido bien los datos PHP");
        }
    })



});