$("#btnReg").on('click', function (){

    // AQUÍ T0DO ES COMO ANTES, COJO LOS VALORES Y LOS ENVÍO A PHP
    event.preventDefault();
    $.ajax({

        method: "post",
        url:"SaintSeiyaRegister.php",
        data:{"usuariomail": $("#emailInput").val(), "usuariopw": $("#pwInput").val()},
        dataType: 'json',

        // Y AQUÍ ES CUANDO PHP ME DEVUELVE LA DATA Y TENGO QUE HACER ALGO CON ELLA....
        success: function (data) {
            if (data["estat"] == "KO"){
                $("#respuesta").css("color", "red");
                console.log(data);
                $("#respuesta").text("ERROR: "+data["error"]+" --> "+ data["usuari"]);
            } else {
                $("#respuesta").css("color", "green");
                console.log(data);
                $("#respuesta").text("OK " + data["usuari"]);
            }
        },
        error: function(jqXHR, textStatus, error){
            alert("Error: "+textStatus+" "+error);
        }
    })
});




