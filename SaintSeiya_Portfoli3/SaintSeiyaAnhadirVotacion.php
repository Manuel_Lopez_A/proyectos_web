<?php
//traemos los valores que hemos capturado con AJAX:


//preparamos valores de la conexión
$servername = "localhost";
$username = "root";
$password = "super3";
$dbname = "webanimes";


try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //VOTACION ALEATORIA---------
    $votacion = rand(0, 10);

    //TOTAL USUARIOS : 10
    $sql1 = $conn->prepare("SELECT * FROM users");
    $sql1->execute();
    //USUARIO ALEATORIO--------
    $usuarioVotante = rand(1, ($sql1->rowCount()));


    //TOTAL ANIMES: 18
    $sql2 = $conn->prepare("SELECT * FROM animes");
    $sql2->execute();

    //ANIME ALEATORIO--------
    $animeVotado = rand(1, ($sql2->rowCount()));


    $BUSQUEDA = $conn->prepare("SELECT * FROM usuarianime where idusuari = $usuarioVotante");
    $BUSQUEDA->execute();

    //select nVotos
    $buscaVotos = $conn->prepare("SELECT NumeroVots FROM animes where idAnimes = $animeVotado");
    $buscaVotos->execute();
    $nVotaciones = $buscaVotos->fetch(PDO::FETCH_ASSOC);

    //select puntsTotals
    $buscaPuntos = $conn->prepare("SELECT PuntsTotals FROM animes where idAnimes = $animeVotado");
    $buscaPuntos->execute();
    $puntuacionTotal = $buscaPuntos->fetch(PDO::FETCH_ASSOC);


    //SI NO EXISTE EL USUARIANIME LO INSERTAMOS
    if ($BUSQUEDA->rowCount() == 0) {
        //insert

        $sql = "INSERT INTO usuarianime ( idusuari , usuarianimeratings) VALUES ('$usuarioVotante','[{\"anime\":$animeVotado,\"rating\":$votacion}]')";
        $conn->exec($sql);


        //devolver json
        print_r(json_encode([
            "estat" => "OK",
            "votacion" => "$votacion",
            "anime" => "$animeVotado",
            "user" => "$usuarioVotante"
        ]));

    } else {

        $BUSQUEDA2 = $conn->prepare("SELECT usuarianimeratings FROM usuarianime where idusuari = $usuarioVotante");
        $BUSQUEDA2->execute();
        $listaVotos = $BUSQUEDA2->fetch(PDO::FETCH_ASSOC);

//        -----------------------------------------------------
//    print_r(json_encode($listaVotos["usuarianimeratings"]));  //<----------- es el String '[{"anime":9, "rating":4}]'

        if (str_contains($listaVotos["usuarianimeratings"], "\"anime\":$animeVotado")) {
            print_r(json_encode([
                "estat" => "KO",
                "error" => "VOTACIÓN DUPLICADA",
                "anime" => "$animeVotado",
                "user" => "$usuarioVotante"
            ]));

        } else {
            $nouString = substr_replace($listaVotos["usuarianimeratings"], ",{\"anime\":$animeVotado,\"rating\":$votacion}", strlen($listaVotos["usuarianimeratings"]) - 1, 0);

            $BUSQUEDA3 = $conn->prepare("UPDATE usuarianime SET usuarianimeratings = '$nouString' WHERE idusuari = $usuarioVotante ");
            $BUSQUEDA3->execute();

//  UPDATE ANIME::::::::::::::::::::::::
//  coger anime y meterle :
            $nV = $nVotaciones["NumeroVots"] + 1;
            $pT = $puntuacionTotal["PuntsTotals"]+ $votacion;
//  hacer update en ficha de anime

            $updateAnime = $conn->prepare("UPDATE animes SET NumeroVots = $nV, PuntsTotals = $pT WHERE idAnimes = $animeVotado");
            $updateAnime->execute();

            print_r(json_encode([
                    "estat" => "LISTA VOTACIONES ACTUALIZADA",
                    "anime" => "$animeVotado",
                    "user" => "$usuarioVotante",
                    "votacion" => "$votacion",
                    "nVotosTotal"=> "$nV",
                    "puntuacionTotal"=> "$pT"
                ]
            ));
        }
    }





} catch (PDOException $e) {
    // Devolvemos KO y el error que ha producido
    echo(json_encode([
        "estat" => "KO",
        "error" => "erro de conexión a la BDD",
        "usuari" => "PELELE"
    ]));
}

$conn = null;



