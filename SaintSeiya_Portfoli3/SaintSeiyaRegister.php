<?php
//traemos los valores que hemos capturado con AJAX:
$mail = $_POST["usuariomail"];
$pw = $_POST["usuariopw"];

//preparamos valores de la conexión
$servername = "localhost";
$username = "root";
$password = "super3";
$dbname = "webanimes";
$table = "users";


try {
    // Con esto hace la conexión
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //prepara la consulta SQL para buscar el usuario
    $exists = $conn->prepare("SELECT * FROM users WHERE nom='{$mail}'");
    $exists->execute();


    if ($exists->rowCount() > 0) {
        print_r(json_encode([
            "estat" => "KO",
            "error" => "User Exists",
            "usuari" => $mail
        ]));

    } else {

        if (!str_ends_with($mail, "@ies-sabadell.cat")) {
            // El email está mal
            print_r(json_encode([
                "estat" => "KO",
                "error" => "Mail not part of email",
                "usuari" => $mail
            ]));
        } else {
            $sql = "INSERT INTO $table ( nom, password) VALUES ('$mail', '$pw')";
            $conn->exec($sql);

            print_r(json_encode([
                "estat" => "OK",
                "error" => "",
                "usuari" => $mail,
            ]));

        }
    }

} catch (PDOException $e) {
    // Devolvemos KO y el error que ha producido
    echo(json_encode([
        "estat" => "KO",
        "error" => "error de conexión a la BDD",
        "usuari" => $mail
    ]));
}


$conn = null;


