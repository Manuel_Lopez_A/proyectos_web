// generar tiempo SCRIPTS

function generateTime(){
    let timeNow = new Date();


    let days = ["Domingo", "Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"];
    let month = ["Enero", "Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto", "Septiembre","Octubre","Noviembre","Diciembre"];
    let hours = timeNow.getHours() < 10 ? "0" + timeNow.getHours() : timeNow.getHours();
    let minutes = timeNow.getMinutes() < 10 ? "0" + timeNow.getMinutes() : timeNow.getMinutes();
    let seconds = timeNow.getSeconds() < 10 ? "0" + timeNow.getSeconds() : timeNow.getSeconds();
    let dia = days[timeNow.getDay()];
    let mes = month[timeNow.getMonth()];
    let año = timeNow.getFullYear();
    let diaNum = timeNow.getDate();


    document.getElementById("time").innerHTML = dia + " "+ diaNum + " de " + mes + " de "+ año + " -&#45;&#45; "+ hours +":"+ minutes + ":"+ seconds;
}

setInterval(() => {
    generateTime();
}, 1000);