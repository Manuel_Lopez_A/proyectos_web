import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';  // importar FormsModule


import { ColorsComponent } from './view/colors/colors.component';
import { ImatgeComponent } from './view/imatge/imatge.component';
import { TraductorEgipciComponent } from './view/traductor-egipci/traductor-egipci.component';
import { SeriesComponent } from './view/series/series.component';
import { HomeComponent } from './view/home/home.component';
import { ExerciciEgipciFill1Component} from './view/exercici-egipci-fill1/exercici-egipci-fill1.component';
import { ExerciciEgipciFill2Component} from "./view/exercici-egipci-fill2/exercici-egipci-fill2.component";


@NgModule({
  declarations: [
    AppComponent,
    ColorsComponent,
    ImatgeComponent,
    TraductorEgipciComponent,
    SeriesComponent,
    HomeComponent,
    ExerciciEgipciFill1Component,
    ExerciciEgipciFill2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,   // agregar FormsModule a las importaciones
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
