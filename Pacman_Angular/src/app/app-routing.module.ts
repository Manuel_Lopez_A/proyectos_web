import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

import { GameComponent } from "./view/game/game.component";
import { HelpComponent } from "./view/help/help.component";
import { RankingComponent } from "./view/ranking/ranking.component";
import { LoginComponent } from "./view/login/login.component";
import { RegisterComponent } from "./view/register/register.component";
import { HomeComponent } from "./view/home/home.component";


// importamos aquí los componentes y le decimos las rutas...
const routes : Routes = [

  { path: 'Home', component: HomeComponent },
  { path: 'Juego', component: GameComponent },
  { path: 'Ayuda', component: HelpComponent },
  { path: 'Login', component: LoginComponent },
  { path: 'Registro', component: RegisterComponent },
  { path: 'Clasificacion', component: RankingComponent }
];

@NgModule({
  declarations: [],
  exports: [RouterModule],     // por qué se tiene que exportar??
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)   // esto qué ES???
  ]
})
export class AppRoutingModule { }
