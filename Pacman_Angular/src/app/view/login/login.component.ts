import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  constructor(private connectbd: ConnectBDService) {}

  data: any;
  usuari = new FormControl("");
  contrasenya = new FormControl("");

  checkUserFunction(){
    this.connectbd.checkUserAPI(this.usuari, this.contrasenya).subscribe(res=> {
      console.log(res);
      if (res.data.length == 0){
        this.data=[];
      }
      else{
        this.data = res.data;
      }
    })
  }
}
