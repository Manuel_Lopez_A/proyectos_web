import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  constructor(private connectbd: ConnectBDService) { }
  data: any;
  registerUser!: FormGroup;
  message: string="";

  ngOnInit():void{
    this.registerUser = new FormGroup({
      id: new FormControl(""),
      nom: new FormControl(""),
      password: new FormControl("")
    })
  }
  
  get id(){
    return this.registerUser.get('id');
  }
  get nom(){
    return this.registerUser.get('nom');
  }
  get password(){
    return this.registerUser.get('password');
  }

  registerUserFunction(){
    this.connectbd.addNewUserAPI(this.registerUser.value)
      .subscribe(res=>{
        this.message = res['message'];
      })
  }
}
