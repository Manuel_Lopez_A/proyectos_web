import { Component, HostListener } from '@angular/core';
import { ViewportScroller } from "@angular/common";
//import { LoginServiceService } from "../../services/login-service.service";
import {ConnectBDService} from "../../shared/services/connect-bd.service";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})

export class GameComponent {
  grid = [
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'pacman', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'fruita', 'fruita', 'fruita', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'wall', 'wall', 'fruita', 'ghost', 'ghost', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
  ]
  pos_fila=2
  pos_columna=1
  casella=this.grid[this.pos_fila][this.pos_columna]
  id=0

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    /* Gestionar moviments del pacman*/
    switch (event.code)
    {
      case 'ArrowUp':

        /* Tecla Fletxa amunt */
        console.log("arrow up");
        console.log(this.grid);

        /* llegir casella destí */
        this.casella = this.grid[this.pos_fila-1][this.pos_columna];

        /* Què tenim a la casella destí? */
        if (this.casella != 'wall')
        {
          if(this.casella=='fruita'){
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna]='';

            /* Situal el pacman a la següent casella */
            this.pos_fila--;
            this.grid[this.pos_fila][this.pos_columna]='pacman';

            /* ToDo: incrementar puntuació pq fruita menjada */

          }else if(this.casella=='ghost'){
            /* ToDo */
          }else{
            /* ToDo */
          }
        }
        break;

      case 'ArrowDown':
        console.log("arrow down");
        /* ToDo: fer quelcom equivalent al cas ArrowUp*/
        break;

      case 'ArrowLeft':
        console.log("arrow left");
        /* ToDo: fer quelcom equivalent al cas ArrowUp*/
        break;

      case 'ArrowRight':
        console.log("arrow right");
        /* ToDo: fer quelcom equivalent al cas ArrowUp*/
        break;

      default:
        break;
    }

  }

  /**
   * Funció per fer el moviment dels fantasmes.
   */
  moureFantasma(){
    /* ToDo.
       Idea:
       - generar número aleatori entre 1 i 4, que són les 4 direccions possibles a les quals es podrà moure el fantasma
       - fer un switch de la direcció aleatòria resultant i en cada cas moure el fantasma a la posició resultant i avaluar si el fantasma es menja al pacman
     */
  }

  /* Per evitar que el cursor mogui el navegador */
  constructor(private viewportScroller: ViewportScroller, /* private loginservice: LoginServiceService, */  private connectbd: ConnectBDService) { }
  arrowKeysPressed = {
    ArrowUp: false,
    ArrowDown: false,
    ArrowLeft: false,
    ArrowRight: false
  };


  ngOnInit() {
    //...
    window.addEventListener('scroll', () => {
      if (this.arrowKeysPressed.ArrowUp || this.arrowKeysPressed.ArrowDown || this.arrowKeysPressed.ArrowLeft || this.arrowKeysPressed.ArrowRight) {
        window.scrollTo(0, 200);
      }
    });
    window.addEventListener('keyup', (event) => {
      if (event.code in this.arrowKeysPressed) {
        window.scrollTo(0, 200);
      }
    });


    /*
      Enunciat: Un cop que ja tenim el moviment del pacman, el següent pas, és generar el
      moviment dels fantasmes de manera automàtica. Una manera és utilitzar la
      funció setInterval de javascript perquè generi un moviment cada x temps.
      Aquesta funció retorna un id que podem utilitzar més endavant.
    */
    this.id = window.setInterval(this.moureFantasma, 1000);
  }

}
