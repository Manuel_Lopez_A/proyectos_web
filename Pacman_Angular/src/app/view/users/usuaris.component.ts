import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";

@Component({
  selector: 'app-usuaris',
  templateUrl: './usuaris.component.html',
  styleUrls: ['./usuaris.component.css']
})
export class UsuarisComponent {
  
  constructor(private connectbd: ConnectBDService) { }
  
  data: any;

  getUsuaris(){
    this.connectbd.getAllUsersAPI().subscribe(res=> {
      console.log(res);
      if (res.data.length==0){
        this.data=[];
      }
      else{
        this.data = res.data;
      }
    })
  }
}
