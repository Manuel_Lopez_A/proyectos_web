/* pas 1: ja fet: crearem un projecte d’Angular en comptes de Node.js. */


/* pas 2: Importar la llibreria Express i inicialitzar una variable amb express.*/
var express = require('express');
var app = express();


/* pas 3: Importar les llibreries de mysql, cors i body-parser. */
var mysql = require('mysql');
var cors = require('cors');
var bodyParser = require('body-parser');


/* pas 4: Indiquem que utilitzi cors per evitar problemes amb el navegador. */
app.use(cors());


/* pas 5: Indicar que utilitzarem json i que rebrem peticions amb url. */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


/* pas 6: Indiquem que acceptem peticions pel port 3000: */
// set port
app.listen(3001, function () {
  console.log('Node app is running on port 3001');
});
module.exports = app;


/* pas 7: Establim la connexió amb la BD */
var dbConn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'super3',
  database: 'webanimes'
});
/* Pas 7 "extra": connectar a la BBDD */
dbConn.connect();


/* pas 8: cal executa el següent script només la primera vegada que fas Node.js amb Express al
mysql:
    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'super3';
*/

<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->


/* pas 9: Creem la petició per defecte /*/
app.get('/', function (req, res) {
 // return res.send({ error: false, message: 'hello' })
  return res.send({message: 'Hola tio'})
});


/* pas 11: Per obtenir tots els animes, fem una petició de la mateixa manera que es feia amb php. */
app.get('/getAllAnimes', function (req, res) {
  dbConn.query('SELECT * FROM animes',
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: "Llista d'animes." });
    });
});


/* Pas 12: Per fer una petició API amb get amb un filtre, es fa de la següent manera */
/* veure comentaris d'ajuda sobre aquest pas a l'enunciat */
app.get('/getAnimesVotsPunts/:vots/:punts', function (req, res) {
  let vots = req.params.vots;
  let punts = req.params.punts;
  dbConn.query('SELECT * FROM animes where NumeroVots>=? and PuntsTotals>=?', [vots,punts],
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Select Ok' });
    });
});


/* Pas 13: Per últim per fer un insert, és molt semblant a la consulta i es fa de la següent manera */
app.get('/setAnimesVotsPunts/:nom/:vots/:punts', function (req, res) {
  let vots = req.params.vots;
  let punts = req.params.punts;
  let nom = req.params.nom;
  dbConn.query('INSERT INTO animes set ?', {Nom:nom, NumeroVots: vots, PuntsTotals:punts},
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Insert Ok' });
    });
});


/* Pas 14: A continuació anem a crear post. Com que aquestes no es poden validar des de URL,
utilitzarem Postman per comprovar que és correcte. La petició post és la següent: */
app.post('/getAnimesVotsPunts2', function (req, res) {
  let vots = parseInt(req.body.vots);
  let punts = parseInt(req.body.punts);

  console.log(vots);
  console.log(punts);

  dbConn.query('SELECT * FROM animes where NumeroVots>=? and PuntsTotals>=?', [vots,punts],
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Select Ok.' });
    });
});


/**********************************************/
/************ ACTIVITATS **********************/
/**********************************************/

/* Activitat 1: Crea una petició get API que retorni tots els usuaris. */
app.get('/getAllUsers', function (req, res) {
  dbConn.query('SELECT * from users',
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: "Llista d'usuaris." });
    });
});

/* Activitat 2: Crea una petició get API que comprovi si l’usuari i la contrasenya són iguals al que hi ha a la BD. */
app.get('/getAnimesCheckUserPassword/:nom/:password', function (req, res) {
  let nom = req.params.nom;
  let password = req.params.password;
  dbConn.query('SELECT * FROM users where nom = ? and password = ?', [nom,password],
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Les dades proporcionades són iguals a les que hi ha a la base de dades' });
    });
});

/* Activitat 3: Crea una petició get API que faci un registre d’un nou usuari. És a dir, que afegeixi un nou registre. */
app.get('/getNouUsuari/:nom/:password/:id', function (req, res) {
  let password = req.params.password;
  let id = req.params.id;
  let nom = req.params.nom;
  /*dbConn.query('INSERT INTO animes set ?', {Nom:nom, NumeroVots: vots, PuntsTotals:punts}*/
  dbConn.query('INSERT INTO users set ? ', {id:id, nom:nom, password:password},
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Insert Ok' });
    });
});

/* Activitat 4: Crea una petició post per les activitats 2 i 3. Mostra una captura que desde Postman retorna dades. */
/* POST de l'activitat 2 (SELECT) */
app.post('/selectPost', function (req, res) {
  let nom = req.body.nom;
  let password = req.body.password;

  dbConn.query('SELECT * FROM users where nom = ? and password = ?', [nom,password],
    function (error, results, fields) {
      if (error) throw error;
      // devuelve
      return res.send({ error: false, data: results, message: 'Select Ok.' });
    });
});

/* POST de l'activitat 3 (INSERT) */
app.post('/insertPost', function (req, res) {
  let id = req.body.id;
  let nom = req.body.nom;
  let password = req.body.password;

  dbConn.query('INSERT INTO users set ?', {id:id, nom:nom, password:password},
    function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Insert Ok.' });
    });
});


<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->
<!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * -->

/* NODE VS ANGULAR - 13 */

app.post('/setAnimesVotsPunts2', function (req,res){
  let vots = parseInt(req.body.vots);
  let punts = parseInt(req.body.punts);
  let nom = req.body.nom;

  dbConn.query("INSERT INTO animes set ?",
    {Nom:nom, NUmeroVots:vots, PuntsTotals:punts}, function (error,results,fields){
    if(error){
      throw error;
    }
    return res.send({error:false, data:results, message:'Se ha insertado OK'})
    });

});
