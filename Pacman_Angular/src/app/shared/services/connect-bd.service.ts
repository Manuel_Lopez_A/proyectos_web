import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
/* esta clase que se crea es para interactuar en cada módulo importandola */
/* es la que hace la conexión contra la base de datos */
export class ConnectBDService {
  REST_API : string ='http://localhost:3001';

  constructor(private httpclient :HttpClient) {}

  public getAllAnimes(): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/getAllAnimes`);
  }

  public getAnimesVotsPunts(vots: FormControl, punts: FormControl): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/getAnimesVotsPunts/${vots.value}/${punts.value}`);
  }

  public setAnime(form: FormGroup): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setAnimesVotsPunts2`, form);
  }

  public getAllUsers(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getAllUsers`);
  }



  /* EXERCICI 2 - GET */
  public getUserExists(nom: FormControl, password: FormControl): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/getAnimesCheckUserPassword/${nom.value}/${password.value}`);
  }

  /* EXERCICI 2 - POST */
  public userExists(form: FormGroup): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/selectPost`, form);
  }



  /*ESTE CÓDIGO ES DE LA AYUDA A LA ACTIVIDAD ANGULAR II
    SON TODAS FUNCIONES PARA CONTROLAR LOGINS Y REGISTERS
   */
  public setUser(form: FormGroup): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/insertPost`, form);
  }

  public getAllUsersAPI(): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/getAllUsers`);
  }

  /* esta es la que he usado para el exercici 2 */
  public checkUserAPI(usuari:FormControl, contrasenya:FormControl): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/check/`+usuari.value+"/"+contrasenya.value);
  }

  public login(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/login`, form);
  }

  public registerAPI(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/register`, form);
  }

  public addNewUserAPI(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/addNewUser`, form);
  }
}
