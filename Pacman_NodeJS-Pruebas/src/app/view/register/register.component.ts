import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../shared/classes/User";
import {emailValidator, nomValidator} from "../login/validar-password.directive";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {

  constructor(private connectdb: ConnectBDService) {   }

  data: any;
  registerForm!: FormGroup;
  message:string="";

  error!:boolean;


  ngOnInit(): void {
    this.registerForm = new FormGroup({
      id : new FormControl('',
        [Validators.required, Validators.minLength(2)]),
      nom : new FormControl('',
        [Validators.required, Validators.minLength(3),
          Validators.email, emailValidator()]),
      password: new FormControl('',
        [Validators.required,
          Validators.minLength(8), nomValidator() ])
    })
  }

  get id(){
    return this.registerForm.get('id');
  }

  get nom() {
    return this.registerForm.get('nom');
  }

  get password() {
    return this.registerForm.get('password');
  }

  postRegister() {
    this.connectdb.registerUserPacman(this.registerForm.value).subscribe(
      res => {
        console.log(res);
        this.message = res['message'];
        if (res['error']){
            this.error = true;
        } else {
          this.error = false;
          /*this.registraEnRanking();*/
        }
        /*
        console.log(res)
        console.log(res.data);
        console.log(res.data.affectedRows);
        */


      }

    )

  }
/*  registraEnRanking(){

  }*/


}
