/* importaciones principales */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConnectBDService} from "../../shared/services/connect-bd.service";
import { RegisterComponent } from './register.component';
/* se importan cuando en el primer beforeEach los incluimos*/
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AppRoutingModule} from "../../app-routing.module";
/* se importan cuando los incluimos en providers */
import {HttpClient} from "@angular/common/http";
import {LoginComponent} from "../login/login.component";
import {of} from "rxjs";



describe('RegisterComponent', () => {

  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let connectbd : ConnectBDService;


  beforeEach(async () => {
    /* FIJARSE EN LO QUE IMPORTAMOS */
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports:[FormsModule, ReactiveFormsModule, HttpClientTestingModule, AppRoutingModule],
      providers:[ConnectBDService, HttpClient]
    })
      .compileComponents(); /* COMPILAMOS TOODO */


    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();

  });


/* CASO DE PRUEBAS PARA EL REGISTER */

  it('REGISTER USER', () => {
    const res = {
      error: false,
      data: { },
      message: "REGISTRO REALIZADO"
    };

    spyOn(connectbd, 'registerUserPacman').and.returnValue(of(res));


    component.registerForm.patchValue({
      id: '169',
      nom: 'emmanuella@ies-sabadell.cat',
      password: "Crucigramas_96"
    });


    component.postRegister();

    fixture.detectChanges();

    /* AQUÍ HACE REFERENCIA AL MESSAGE QUE HAY EN EL HTML, QUE LE ENVÍA EL TS */
    expect(component.message).toEqual(res.message);


  });

  it('ID REPETIDA', () => {

    const res = {
      error: true,
      message: "ERROR: ID REPETIDA"
    };

    spyOn(connectbd, 'registerUserPacman').and.returnValue(of(res));


    component.registerForm.patchValue({
      id: '169',
      nom: 'emmanuella@ies-sabadell.cat',
      password: "Crucigramas_96"
    });


    component.postRegister();

    fixture.detectChanges();

    /* AQUÍ HACE REFERENCIA AL MESSAGE QUE HAY EN EL HTML, QUE LE ENVÍA EL TS */
    expect(component.message).toContain('ERROR: ID REPETIDA');


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
