import {Component, HostListener, Input} from '@angular/core';
import {ViewportScroller} from "@angular/common";


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})

export class GameComponent {
  /* TABLERO */

  grid = [
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'pacman', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'fruita', 'fruita', 'fruita', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
    ['wall', 'wall', 'wall', 'ghost', 'fruita', 'fruita', 'fruita', 'wall', 'ghost', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'fruita', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
    ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
  ]

  /* DIFICULTAD */

  dificultad = 'facil'
  velocidadF = 1000

  /* PACMAN COSAS */
  pos_fila = 2
  pos_columna = 1
  casella = this.grid[this.pos_fila][this.pos_columna]
  id = 0
  isGameOver = false
  haveYouWin = false
  puntos = 0


  // --------------------------------------

  //MOVIDAS DE LOS FANTASMAS

  // FANTASMA 1
  f1f = 6
  f1c = 3
  casillaAnteriorPunto1 = true
  casillaDestinoFantasma1 = this.grid[this.f1f][this.f1c]
  posiblesDirecciones1 = [0, 1, 2, 3]


  // FANTASMA 2
  f2f = 6
  f2c = 8
  casillaAnteriorPunto2 = true
  casillaDestinoFantasma2 = this.grid[this.f2f][this.f2c]
  posiblesDirecciones2 = [0, 1, 2, 3]


  // ------------------------------------------

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {

    /* COMPROBAR WIN OR LOSE */
    this.haveYouWin = this.youWin()
    if (this.isGameOver) {
      // Detener la ejecución del intervalo si el juego ha terminado
      clearInterval(this.id);
      return;
    } else if (this.haveYouWin) {
      clearInterval(this.id);
      return;
    }

    /* PACMAN MOVIMIENTOS*/
    switch (event.code) {
      case 'ArrowUp':

        /* Tecla Fletxa amunt */


        /* llegir casella destí */
        this.casella = this.grid[this.pos_fila - 1][this.pos_columna];

        /* Què tenim a la casella destí? */
        if (this.casella != 'wall') {
          if (this.casella == 'fruita') {


            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_fila--;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';

            /* ToDo: incrementar puntuació pq fruita menjada */
            this.puntua()

          } else if (this.casella == 'ghost') {
            this.grid[this.pos_fila][this.pos_columna] = 'muerte';
            this.gameOver()
          } else {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_fila--;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';
          }
        } else {

        }
        break;

      case 'ArrowDown':
        /* Tecla Fletxa amunt */


        /* llegir casella destí */
        this.casella = this.grid[this.pos_fila + 1][this.pos_columna];

        /* Què tenim a la casella destí? */
        if (this.casella != 'wall') {
          if (this.casella == 'fruita') {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_fila++;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';

            /* ToDo: incrementar puntuació pq fruita menjada */
            this.puntua()

          } else if (this.casella == 'ghost') {
            this.grid[this.pos_fila][this.pos_columna] = 'muerte';
            this.gameOver()
          } else {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_fila++;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';
          }
        }
        break;

      case 'ArrowLeft':
        /* Tecla Fletxa amunt */


        /* llegir casella destí */
        this.casella = this.grid[this.pos_fila][this.pos_columna - 1];

        /* Què tenim a la casella destí? */
        if (this.casella != 'wall') {
          if (this.casella == 'fruita') {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_columna--;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';

            /* ToDo: incrementar puntuació pq fruita menjada */
            this.puntua()

          } else if (this.casella == 'ghost') {
            this.grid[this.pos_fila][this.pos_columna] = 'muerte';
            this.gameOver()
          } else {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_columna--;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';
          }
        }
        break;

      case 'ArrowRight':
        /* Tecla Fletxa amunt */


        /* llegir casella destí */
        this.casella = this.grid[this.pos_fila][this.pos_columna + 1];

        /* Què tenim a la casella destí? */
        if (this.casella != 'wall') {
          if (this.casella == 'fruita') {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_columna++;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';

            /* ToDo: incrementar puntuació pq fruita menjada */
            this.puntua()

          } else if (this.casella == 'ghost') {
            this.grid[this.pos_fila][this.pos_columna] = 'muerte';
            this.gameOver()
          } else {
            /* Casella actual: treure el pacman */
            this.grid[this.pos_fila][this.pos_columna] = '';

            /* Situal el pacman a la següent casella */
            this.pos_columna++;
            this.grid[this.pos_fila][this.pos_columna] = 'pacman';
          }
        }
        break;

      default:
        break;
    }

  }

  /* MOVER FANTASMAS */
  moureFantasma() {
    /* COMPROBAR WIN OR LOSE */
    this.haveYouWin = this.youWin()
    if (this.isGameOver) {
      // Detener la ejecución del intervalo si el juego ha terminado
      clearInterval(this.id);
      return;
    } else if (this.haveYouWin) {
      clearInterval(this.id);
      return;
    }

    // MOVIMIENTOS FANTASMAS*/

    // INTELIGENCIA FANTASMAS
    let nn1 = this.posiblesDirecciones1.length
    let nn2 = this.posiblesDirecciones2.length
    // dependiendo la longitud el random será mayor o menor...
    let n1 = Math.floor(Math.random() * nn1)
    let n2 = Math.floor(Math.random() * nn2)
    // y rebuscamos en el array en la poscion que nos da
    let nf1 = this.posiblesDirecciones1[n1]
    let nf2 = this.posiblesDirecciones2[n2]

    /* MOVIMIENTOS FANTASMA 1 */
    switch (nf1) {
      //subir
      case 0:

        this.casillaDestinoFantasma1 = this.grid[this.f1f - 1][this.f1c]

        if (this.casillaDestinoFantasma1 !== 'wall' && this.casillaDestinoFantasma1 !== 'ghost') {

          if (this.casillaDestinoFantasma1 == 'pacman') {
            //GAME OVER
            this.grid[this.f1f - 1][this.f1c] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma1 == 'fruita') {

            if (this.casillaAnteriorPunto1) {

              this.f1f--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f + 1][this.f1c] = 'fruita'

            } else {

              this.f1f--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f + 1][this.f1c] = ''
              this.casillaAnteriorPunto1 = true

            }
          } else if (this.casillaDestinoFantasma1 == '') {

            if (this.casillaAnteriorPunto1) {

              this.f1f--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f + 1][this.f1c] = 'fruita'
              this.casillaAnteriorPunto1 = false

            } else {

              this.f1f--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f + 1][this.f1c] = ''


            }
          }

        } else {
          // muro, no te muevas
        }

        //borra el array
        this.posiblesDirecciones1 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f1f - 1][this.f1c] !== 'wall' && this.grid[this.f1f - 1][this.f1c] !=='ghost') {
          this.posiblesDirecciones1.push(0)
        }
        if (this.grid[this.f1f][this.f1c - 1] !== 'wall' && this.grid[this.f1f][this.f1c - 1] !== 'ghost') {
          this.posiblesDirecciones1.push(2)
        }
        if (this.grid[this.f1f][this.f1c + 1] !== 'wall' && this.grid[this.f1f][this.f1c + 1] !== 'ghost') {
          this.posiblesDirecciones1.push(3)
        }
        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones1.length === 0) {
          this.posiblesDirecciones1.push(1)
        }


        break;
      //bajar
      case 1:

        this.casillaDestinoFantasma1 = this.grid[this.f1f + 1][this.f1c]

        // si destino no es muro
        if (this.casillaDestinoFantasma1 !== 'wall' && this.casillaDestinoFantasma1 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma1 == 'pacman') {
            //GAME OVER
            this.grid[this.f1f + 1][this.f1c] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma1 == 'fruita') {

            if (this.casillaAnteriorPunto1) {

              this.f1f++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f - 1][this.f1c] = 'fruita'

            } else {

              this.f1f++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f - 1][this.f1c] = ''
              this.casillaAnteriorPunto1 = true

            }
          } else if (this.casillaDestinoFantasma1 == '') {

            if (this.casillaAnteriorPunto1) {

              this.f1f++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f - 1][this.f1c] = 'fruita'
              this.casillaAnteriorPunto1 = false

            } else {

              this.f1f++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f - 1][this.f1c] = ''


            }
          }

        } else {
          //no te muevas porque es muro
        }

        //borra el array
        this.posiblesDirecciones1 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f1f + 1][this.f1c] !== 'wall' && this.grid[this.f1f + 1][this.f1c] !== 'ghost') {
          this.posiblesDirecciones1.push(1)
        }
        if (this.grid[this.f1f][this.f1c - 1] !== 'wall' && this.grid[this.f1f][this.f1c - 1] !== 'ghost') {
          this.posiblesDirecciones1.push(2)
        }
        if (this.grid[this.f1f][this.f1c + 1] !== 'wall' && this.grid[this.f1f][this.f1c + 1] !=='ghost') {
          this.posiblesDirecciones1.push(3)
        }
        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones1.length === 0) {
          this.posiblesDirecciones1.push(0)
        }

        break;
      // izquierda
      case 2:
        this.casillaDestinoFantasma1 = this.grid[this.f1f][this.f1c - 1];

        // si destino no es muro
        if (this.casillaDestinoFantasma1 !== 'wall' && this.casillaDestinoFantasma1 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma1 == 'pacman') {
            //GAME OVER
            this.grid[this.f1f][this.f1c - 1] = 'muerte'
            this.gameOver()

          }
          else if (this.casillaDestinoFantasma1 == 'fruita') {

            if (this.casillaAnteriorPunto1) {

              this.f1c--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c + 1] = 'fruita'

            } else {

              this.f1c--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c + 1] = ''
              this.casillaAnteriorPunto1 = true

            }
          }
          else if (this.casillaDestinoFantasma1 == '') {

            if (this.casillaAnteriorPunto1) {

              this.f1c--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c + 1] = 'fruita'
              this.casillaAnteriorPunto1 = false

            } else {

              this.f1c--
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c + 1] = ''


            }
          }

        } else {
          // no te muevas porque es muro
        }
        //borra el array
        this.posiblesDirecciones1 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f1f - 1][this.f1c] !== 'wall' && this.grid[this.f1f - 1][this.f1c] !== 'ghost') {
          this.posiblesDirecciones1.push(0)
        }
        if (this.grid[this.f1f + 1][this.f1c] !== 'wall' && this.grid[this.f1f + 1][this.f1c] !== 'ghost') {
          this.posiblesDirecciones1.push(1)
        }
        if (this.grid[this.f1f][this.f1c - 1] !== 'wall' && this.grid[this.f1f][this.f1c - 1] !== 'ghost') {
          this.posiblesDirecciones1.push(2)
        }

        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones1.length === 0) {
          this.posiblesDirecciones1.push(3)
        }

        break;
      // derecha
      case 3:
        this.casillaDestinoFantasma1 = this.grid[this.f1f][this.f1c + 1];

        // si destino no es muro
        if (this.casillaDestinoFantasma1 !== 'wall' && this.casillaDestinoFantasma1 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma1 === 'pacman') {
            // GAME OVER
            this.grid[this.f1f][this.f1c + 1] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma1 == 'fruita') {

            if (this.casillaAnteriorPunto1) {

              this.f1c++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c - 1] = 'fruita'

            } else {

              this.f1c++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c - 1] = ''
              this.casillaAnteriorPunto1 = true

            }
          } else if (this.casillaDestinoFantasma1 == '') {

            if (this.casillaAnteriorPunto1) {

              this.f1c++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c - 1] = 'fruita'
              this.casillaAnteriorPunto1 = false

            } else {

              this.f1c++
              this.grid[this.f1f][this.f1c] = 'ghost'
              this.grid[this.f1f][this.f1c - 1] = ''


            }
          }

        } else {
          // no te muevas porque es muro
        }

        //borra el array
        this.posiblesDirecciones1 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f1f - 1][this.f1c] !== 'wall' && this.grid[this.f1f - 1][this.f1c] !== 'ghost') {
          this.posiblesDirecciones1.push(0)
        }
        if (this.grid[this.f1f + 1][this.f1c] !== 'wall' && this.grid[this.f1f + 1][this.f1c] !== 'ghost') {
          this.posiblesDirecciones1.push(1)
        }
        if (this.grid[this.f1f][this.f1c + 1] !== 'wall' && this.grid[this.f1f][this.f1c + 1] !== 'ghost') {
          this.posiblesDirecciones1.push(3)
        }

        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones1.length === 0) {
          this.posiblesDirecciones1.push(2)
        }
        break;
    }
    /* MOVIMIENTOS FANTASMA 2 */
    switch (nf2) {
      //subir
      case 0:
        this.casillaDestinoFantasma2 = this.grid[this.f2f - 1][this.f2c]

        if (this.casillaDestinoFantasma2 !== 'wall' && this.casillaDestinoFantasma2 !== 'ghost') {

          if (this.casillaDestinoFantasma2 == 'pacman') {
            //GAME OVER
            this.grid[this.f2f - 1][this.f2c] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma2 == 'fruita') {

            if (this.casillaAnteriorPunto2) {

              this.f2f--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f + 1][this.f2c] = 'fruita'

            } else {

              this.f2f--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f + 1][this.f2c] = ''
              this.casillaAnteriorPunto2 = true

            }
          } else if (this.casillaDestinoFantasma2 == '') {

            if (this.casillaAnteriorPunto2) {

              this.f2f--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f + 1][this.f2c] = 'fruita'
              this.casillaAnteriorPunto2 = false

            } else {

              this.f2f--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f + 1][this.f2c] = ''


            }
          }

        } else {
          // muro, no te muevas
        }

        //borra el array
        this.posiblesDirecciones2 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f2f - 1][this.f2c] !== 'wall' && this.grid[this.f2f - 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(0)
        }
        if (this.grid[this.f2f][this.f2c - 1] !== 'wall' && this.grid[this.f2f][this.f2c - 1] !== 'ghost') {
          this.posiblesDirecciones2.push(2)
        }
        if (this.grid[this.f2f][this.f2c + 1] !== 'wall' && this.grid[this.f2f][this.f2c + 1] !== 'ghost') {
          this.posiblesDirecciones2.push(3)
        }
        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones2.length === 0) {
          this.posiblesDirecciones2.push(1)
        }


        break;
      //bajar
      case 1:

        this.casillaDestinoFantasma2 = this.grid[this.f2f + 1][this.f2c]

        // si destino no es muro
        if (this.casillaDestinoFantasma2 !== 'wall' && this.casillaDestinoFantasma2 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma2 == 'pacman') {
            //GAME OVER
            this.grid[this.f2f + 1][this.f2c] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma2 == 'fruita') {

            if (this.casillaAnteriorPunto2) {

              this.f2f++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f - 1][this.f2c] = 'fruita'

            } else {

              this.f2f++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f - 1][this.f2c] = ''
              this.casillaAnteriorPunto2 = true

            }
          } else if (this.casillaDestinoFantasma2 == '') {

            if (this.casillaAnteriorPunto2) {

              this.f2f++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f - 1][this.f2c] = 'fruita'
              this.casillaAnteriorPunto2 = false

            } else {

              this.f2f++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f - 1][this.f2c] = ''


            }
          }

        } else {
          //no te muevas porque es muro
        }

        //borra el array
        this.posiblesDirecciones2 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f2f + 1][this.f2c] !== 'wall' && this.grid[this.f2f + 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(1)
        }
        if (this.grid[this.f2f][this.f2c - 1] !== 'wall' && this.grid[this.f2f][this.f2c - 1] !== 'ghost') {
          this.posiblesDirecciones2.push(2)
        }
        if (this.grid[this.f2f][this.f2c + 1] !== 'wall' && this.grid[this.f2f][this.f2c + 1] !== 'ghost' ) {
          this.posiblesDirecciones2.push(3)
        }
        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones2.length === 0) {
          this.posiblesDirecciones2.push(0)
        }

        break;
      // izquierda
      case 2:
        this.casillaDestinoFantasma2 = this.grid[this.f2f][this.f2c - 1];

        // si destino no es muro
        if (this.casillaDestinoFantasma2 !== 'wall' && this.casillaDestinoFantasma2 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma2 === 'pacman') {
            //GAME OVER
            this.grid[this.f2f][this.f2c - 1] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma2 == 'fruita') {

            if (this.casillaAnteriorPunto2) {

              this.f2c--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c + 1] = 'fruita'

            } else {

              this.f2c--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c + 1] = ''
              this.casillaAnteriorPunto2 = true

            }
          } else if (this.casillaDestinoFantasma2 == '') {

            if (this.casillaAnteriorPunto2) {

              this.f2c--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c + 1] = 'fruita'
              this.casillaAnteriorPunto2 = false

            } else {

              this.f2c--
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c + 1] = ''


            }
          }

        } else {
          // no te muevas porque es muro
        }
        //borra el array
        this.posiblesDirecciones2 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f2f - 1][this.f2c] !== 'wall' && this.grid[this.f2f - 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(0)
        }
        if (this.grid[this.f2f + 1][this.f2c] !== 'wall' && this.grid[this.f2f + 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(1)
        }
        if (this.grid[this.f2f][this.f2c - 1] !== 'wall' && this.grid[this.f2f][this.f2c - 1] !== 'ghost') {
          this.posiblesDirecciones2.push(2)
        }

        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones2.length === 0) {
          this.posiblesDirecciones2.push(3)
        }

        break;
      // derecha
      case 3:
        this.casillaDestinoFantasma2 = this.grid[this.f2f][this.f2c + 1];


        // si destino no es muro
        if (this.casillaDestinoFantasma2 !== 'wall' && this.casillaDestinoFantasma2 !== 'ghost') {
          // si es pacman
          if (this.casillaDestinoFantasma2 === 'pacman') {
            //GAME OVER
            this.grid[this.f2f][this.f2c + 1] = 'muerte'
            this.gameOver()
          } else if (this.casillaDestinoFantasma2 == 'fruita') {

            if (this.casillaAnteriorPunto2) {

              this.f2c++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c - 1] = 'fruita'

            } else {

              this.f2c++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c - 1] = ''
              this.casillaAnteriorPunto2 = true

            }
          } else if (this.casillaDestinoFantasma2 == '') {

            if (this.casillaAnteriorPunto2) {

              this.f2c++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c - 1] = 'fruita'
              this.casillaAnteriorPunto2 = false

            } else {

              this.f2c++
              this.grid[this.f2f][this.f2c] = 'ghost'
              this.grid[this.f2f][this.f2c - 1] = ''


            }
          }

        } else {
          // no te muevas porque es muro
        }

        //borra el array
        this.posiblesDirecciones2 = []
        //mira alrededor y si no hay muros metelo en el array, pero la direccion contraria a la que voy NO
        if (this.grid[this.f2f - 1][this.f2c] !== 'wall' && this.grid[this.f2f - 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(0)
        }
        if (this.grid[this.f2f + 1][this.f2c] !== 'wall' && this.grid[this.f2f + 1][this.f2c] !== 'ghost') {
          this.posiblesDirecciones2.push(1)
        }
        if (this.grid[this.f2f][this.f2c + 1] !== 'wall' && this.grid[this.f2f][this.f2c + 1] !== 'ghost') {
          this.posiblesDirecciones2.push(3)
        }

        // pero si me quedo arrinconado tengo que poder volver a salir
        if (this.posiblesDirecciones2.length === 0) {
          this.posiblesDirecciones2.push(2)
        }
        break;
    }


  }

  /* SUMA PUNTOS Y COMPRUEBA SI QUEDAN PUNTOS EN TABLERO*/
  puntua() {
    if (this.dificultad === 'facil') {
      this.puntos += 10
    } else if (this.dificultad === 'media') {
      this.puntos += 20
    } else if (this.dificultad === 'dificil') {
      this.puntos += 40
    } else if (this.dificultad === 'expert') {
      this.puntos += 70
    }
  }

  /* COMPRUEBA SI QUEDAN PUNTOS EN EL TABLERO, SI NO QUEDAN CAMBIA EL HAVEYOUWIN A TRUE */
  youWin() {
    let puntosRestantes = 0
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[0].length; j++) {
        if (this.grid[i][j] === 'fruita') {
          puntosRestantes++
        }
      }
    }
    if (puntosRestantes === 0) {
      console.log('has ganado')
      return true
    } else {
      return false
    }
  }

  /* GAME OVER COMPROBAR */
  gameOver() {

    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[0].length; j++) {
        if (this.grid[i][j] === 'muerte') {
          this.isGameOver = true
        }
      }
    }
  }

  /* REINICIAR VARIABLES */
  reiniciarVariables() {

    clearInterval(this.id);
    this.cambiarDificultad()

    this.haveYouWin = false
    this.isGameOver = false
    /* PACMAN COSAS */
    this.pos_fila = 2
    this.pos_columna = 1
    this.casella = this.grid[this.pos_fila][this.pos_columna]
    this.id = 0
    this.isGameOver = false
    this.haveYouWin = false
    this.puntos = 0



    // --------------------------------------

    //MOVIDAS DE LOS FANTASMAS

    // FANTASMA 1
    this.f1f = 6
    this.f1c = 3
    this.casillaAnteriorPunto1 = true
    this.casillaDestinoFantasma1 = this.grid[this.f1f][this.f1c]
    this.posiblesDirecciones1 = [0, 1, 2, 3]


    // FANTASMA 2
    this.f2f = 6
    this.f2c = 8
    this.casillaAnteriorPunto2 = true
    this.casillaDestinoFantasma2 = this.grid[this.f2f][this.f2c]
    this.posiblesDirecciones2 = [0, 1, 2, 3]


  }

  /* REINICIAR TABLERO */
  reiniciarTablero() {
    this.grid = [
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
      ['wall', 'pacman', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
      ['wall', 'fruita', 'wall', 'fruita', 'fruita', 'fruita', 'fruita', 'wall', 'fruita', 'wall'],
      ['wall', 'fruita', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
      ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'wall', 'fruita', 'wall'],
      ['wall', 'wall', 'wall', 'ghost', 'fruita', 'fruita', 'fruita', 'wall', 'ghost', 'wall'],
      ['wall', 'fruita', 'fruita', 'fruita', 'wall', 'wall', 'fruita', 'fruita', 'fruita', 'wall'],
      ['wall', 'fruita', 'wall', 'wall', 'wall', 'fruita', 'wall', 'wall', 'fruita', 'wall'],
      ['wall', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'fruita', 'wall'],
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
    ]
  }

  /* REINICIAR JUEGO */
  reiniciarJuego() {

    /* REINICIAR VARIABLES */
    this.reiniciarVariables()
    /* REINICIAR TABLERO */
    this.reiniciarTablero()
    /* LIMPIAR INTERVAL */
    clearInterval(this.id);
    /* REINICIO DIFICULTAD */
    this.cambiarDificultad()

    /* REINICIAR MOVIMIENTO FANTASMAS */
    console.log('dificultad desde reiniciar : ' + this.dificultad + ' +++ velocidad Fantasmas: '+this.velocidadF)
     this.id = window.setInterval(this.moureFantasma.bind(this), this.velocidadF);
    // console.log('REINICIAR JUEGO FUNCTION--> nivel: '+ this.dificultad+'//// velocidad fantasmas: '+1000)

  }

  /* CAMBIAR DIFICULTAD */
  cambiarDificultad() {
    console.log(this.dificultad)
    if (this.dificultad === 'facil') {
      this.velocidadF = 1000
    } else if (this.dificultad==='media'){
      this.velocidadF = 500
    } else if (this.dificultad==='dificil'){
      this.velocidadF = 250
    } else if (this.dificultad ==='expert'){
      this.velocidadF = 100
    }
  }

  /* Per evitar que el cursor mogui el navegador (NO SIRVE DE NADA) */
  // constructor(private viewportScroller: ViewportScroller, private loginservice: LoginServiceService, private connectbd: ConnectBDService) { }
  constructor(private viewportScroller: ViewportScroller) {

  }
  arrowKeysPressed = {
    ArrowUp: false,
    ArrowDown: false,
    ArrowLeft: false,
    ArrowRight: false
  };

  ngOnInit() {
    console.log()
    /* VELOCIDAD PACMAN */
    window.addEventListener('scroll', () => {
      if (this.arrowKeysPressed.ArrowUp || this.arrowKeysPressed.ArrowDown || this.arrowKeysPressed.ArrowLeft || this.arrowKeysPressed.ArrowRight) {
        window.scrollTo(0, 300);
      }
    });
    window.addEventListener('keyup', (event) => {
      if (event.code in this.arrowKeysPressed) {
        window.scrollTo(0, 300);
      }
    });


    // /* EVENTO FANTASMAS */
    console.log('dificultad desde ngOnInit : ' + this.dificultad + ' +++ velocidad de los fantasmas: '+this.velocidadF)
    this.id = window.setInterval(this.moureFantasma.bind(this), this.velocidadF);
    // console.log('CAMBIAR DIFICULTAD FUNCTION--> nivel: '+ this.dificultad+'//// velocidad fantasmas: '+this.velocidadF)

    //window.setInterval(this.moureFantasma.bind(this), 1000);
    //this.id = window.setInterval(this.moureFantasma, 1000);
  }

}
