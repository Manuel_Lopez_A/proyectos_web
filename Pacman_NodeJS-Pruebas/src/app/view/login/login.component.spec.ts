import { ComponentFixture, TestBed } from '@angular/core/testing';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import { LoginComponent } from './login.component';
import {HttpClient} from "@angular/common/http";
import {of} from "rxjs";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing"
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppRoutingModule} from "../../app-routing.module";

describe('LoginComponent', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let connectbd: ConnectBDService;

  /* 1  BEFORE-EACH */
  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule, AppRoutingModule],
      providers: [ConnectBDService, HttpClient]
    })
      .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    /* Exemple de cas de proves, en què s’inicialitza una variable abans de començar el test */
    /* component.OK = true;*/
    fixture.detectChanges();
  });
  /* FIN PRIMER BEFORE-EACH */

  /* 2  BEFORE-EACH */
  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });


  /* CASO DE PRUEBAS PARA EL LOGIN */
  it('LOGUEAR USUARIO', () => {
    /*
    ** Pas 1)
    **  data:    dades que "forçarem" que retorni el servei de base de dades (al marge dels valors reals que existeixin a la base de dades)
    **  message: escrivim la resposta en cas que el test sigui OK
    */
    const res = {
      data: [{nom: 'marc@ies-sabadell.cat', password: "Tetris_123"}],
      message: "L'usuari marc@ies-sabadell.cat s'ha loguejat de manera correcta!!!"
    };
    /* Pas 2) "espiem" el servei de base de dades per a interceptar la crida i modificar el resultat retornat amb les dades del pas 1) */
    /* PONEMOS EL NOMBRE DE LA FUNCIÓN QUE HAY EN CONNECTBDSERVICE ==> 'login'  */
    spyOn(connectbd, 'login').and.returnValue(of(res));

    /* Pas 3) preparem login amb els valors a testejar */
    component.loginForm.patchValue({
      nom: 'marc@ies-sabadell.cat',
      password: "Tetris1234_"
    });

    /* Pas 4) Fem el submit */
    /* CLICKAMOS EN EL BOTON QUE LLAMA A 'postLogin' */
    component.postLogin();

    /* Pas 5) Indiquem que detecti els canvis */
    fixture.detectChanges();

    /* Pas 6) últim pas: avaluem resultat contra valor esperat */
    expect(component.message).toEqual(res.message);


  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });







});
