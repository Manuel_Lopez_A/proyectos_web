import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, ValidatorFn} from "@angular/forms";


/* Pas 7: El següent codi, el que fa és associar els serveis. Informa que ell mateix és un servei i
NG_VALIDATORS per implementar validacions. Després, es desenvolupa la classe de
validacions heretant Validator i implementant els mètodes de l’herència. En aquest cas,
és important desenvolupar validate perquè retornarà el resultat del mètode que fa la
validació. */
@Directive({
  selector: '[appValidarPassword]',
  providers :[{provide: NG_VALIDATORS,
  useExisting: ValidarPasswordDirective, multi: true}]
})
export class ValidarPasswordDirective {
  @Input('appName') passwordStrength = false;

  validate (control: AbstractControl): ValidationErrors | null {
    return this.passwordStrength ? nomValidator()(control)
      : null;
  }
    constructor() {}
}

/* Pas 7: A la directiva que valida el password, té la una funció que valida que la contrasenya
tingui majúscules, mínuscules i nombres.*/
export function nomValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      return null;
    }
    const hasUpperCase = /[A-Z]+/.test(value);
    const hasLowerCase = /[a-z]+/.test(value);
    const hasNumeric = /[0-9]+/.test(value);
    const hasSpecial = /[-_]+/.test(value);
    const passwordValid = hasUpperCase && hasLowerCase && hasNumeric && hasSpecial;
    return !passwordValid? {passwordStrength:true}: null;
  }
}

export function emailValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      return null;
    }
    const endsWithIesSabadellCat = value.endsWith('@ies-sabadell.cat');
    return !endsWithIesSabadellCat ? { emailFormat: true }: null;
  };
}
