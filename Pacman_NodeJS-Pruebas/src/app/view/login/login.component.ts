import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../shared/classes/User";
import {nomValidator} from "./validar-password.directive";
import { emailValidator } from "./validar-password.directive";


/**
 * Componente para el inicio de sesión.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  constructor(private connectdb: ConnectBDService) {   }

  /**
   * Formulario de inicio de sesión.
   */
  loginForm!: FormGroup;

  /**
   * Lista de usuarios.
   */
  users : User[] = [];

  /**
   * Mensaje de estado del inicio de sesión.
   */
  message!: string;

  /**
   * Estado de inicio de sesión (true: éxito, false: error).
   */
  OK!: boolean;

  /**
   * Inicializa el componente y crea el formulario de inicio de sesión.
   */
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      nom : new FormControl('',
        [Validators.required, Validators.minLength(3),
          Validators.email, emailValidator()]),
      password: new FormControl('',
        [Validators.required,
          Validators.minLength(8), nomValidator() ])
    })
  }

  /**
   * Obtiene el control del formulario para el campo "nom" (nombre de usuario).
   * @returns  Control del formulario para el campo "nom".
   */
  get nom() {
    return this.loginForm.get('nom');
  }

  /**
   * Obtiene el control del formulario para el campo "password" (contraseña).
   * @returns Control del formulario para el campo "password".
   */
  get password() {
    return this.loginForm.get('password');
  }

  /**
   * Realiza la conexión con la base de datos y realiza el inicio de sesión del usuario.
   * Si el usuario no se encuentra, se muestra un mensaje de error.
   */
  postLogin() {
    this.connectdb.login(this.loginForm.value).subscribe(res => {
      if (res.data.length == 0) {
        this.users = [];
        this.message = "Login incorrecte. ";
        this.OK = false;
      } else {
        this.users = res.data;
        this.message = "L'usuari " + this.users[0].nom
          + " s'ha loguejat de manera correcta!!!";
        this.OK = true;
      }
    })
  }

}
