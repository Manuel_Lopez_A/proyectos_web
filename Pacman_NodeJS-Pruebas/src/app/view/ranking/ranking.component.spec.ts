import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RankingComponent } from './ranking.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {ConnectBDService} from "../../shared/services/connect-bd.service";


describe('RankingComponent', () => {

  let component: RankingComponent;
  let fixture: ComponentFixture<RankingComponent>;
  let connectbd: ConnectBDService;


  /* 1-FOREACH */
  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [RankingComponent],
      imports: [HttpClientTestingModule],
      providers : [HttpClient]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });



  /* 2-FOREACH */
  beforeEach(() => {
    fixture = TestBed.createComponent(RankingComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });

  /* FALTAN CASOS DE PRUEBAS */

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
