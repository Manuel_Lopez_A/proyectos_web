import {Component} from '@angular/core';
/* 0 - IMPORTAR MOVIDAS */
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


/**
 * Componente para mostrar y descargar un ranking de un VideoJuego
 */
@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})


export class RankingComponent {

  /* 1 - CONSTRUCTOR */
  constructor(private connectbd: ConnectBDService) {
  }

  /* 2 - DATOS QUE MANEJAMOS */
  /**
   * Obtiene el ranking llamando a la API.
   */
  data: any;

  /* 3 - FUNCIÓN QUE LLAMA A LA API */
  /**
   * Realiza una solicitud al servicio de conexión con la base de datos
   * para obtener los datos del ranking.
   *
   * @param res - Respuesta de la API con los datos del ranking.
   */
  getRanking() {
    this.connectbd.getClassificacio().subscribe(res => {
      console.log(res);
      if (res.data.length == 0) {
        this.data = [];
      } else {
        this.data = res.data;
      }
    });
  }

  /* 4 - DESCARGAR PDF */
  /**
   * Descarga el ranking en formato PDF (previamente tenemos que haberlo generado, si no, nos sale en blanco).
   */
  downloadPDF()  {
    /**
     * Obtiene el elemento HTML que contiene los datos a exportar como PDF.
     */
    const DATA: HTMLDivElement = document.getElementById('htmlData') as HTMLDivElement;
    const doc = new jsPDF('p', 'pt', 'a4');
    //Orientation: portrait //Unit: points (también se podría poner 'mm', 'cm', 'm', 'in' o 'px' // Formato: a4
    const options = {
      background: 'white',
      scale: 3
    };

    /**
     * Captura el contenido HTML en forma de imagen utilizando la biblioteca html2canvas.
     *
     * @param canvas - Elemento canvas que contiene la imagen del contenido HTML.
     */
    html2canvas(DATA, options).then((canvas) => {
      const img = canvas.toDataURL('image/PNG');
      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 *
        bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) /
        imgProps.width;
      /**
       * Agrega la imagen del contenido HTML al documento PDF.
       */
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth,
        pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      /**
       * Guarda el documento PDF con un nombre único basado en la fecha y hora actual
       * con la función toISOString (de la clase Date)
       */
      docResult.save(`${new Date().toISOString()}.pdf`);
    });
  }

}
