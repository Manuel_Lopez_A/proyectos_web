import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { GameComponent } from './view/game/game.component';
import { RankingComponent } from './view/ranking/ranking.component';
import { HomeComponent } from './view/home/home.component';
import { HelpComponent } from './view/help/help.component';
import { RegisterComponent} from "./view/register/register.component";
import { LoginComponent} from "./view/login/login.component";


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    RankingComponent,
    HomeComponent,
    HelpComponent,
    RegisterComponent ,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
