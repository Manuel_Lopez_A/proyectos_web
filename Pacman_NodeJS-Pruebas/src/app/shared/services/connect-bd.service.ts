import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
/* esta clase que se crea es para interactuar en cada módulo importandola */
/* es la que hace la conexión contra la base de datos */
export class ConnectBDService {

  REST_API : string ='http://localhost:3001';

  constructor(private httpclient :HttpClient) {}

  /* --------------------------------------------------------------------------------*/
  /* --------------------------------------------------------------------------------*/

  /* PACMAN */



  public getClassificacio(): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/getClassificacio`);
  }





  public login(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/login`, form);
  }

  public registerUserPacman(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/registerUserPacman`, form);
  }



  /* --------------------------------------------------------------------------------*/
  /* --------------------------------------------------------------------------------*/

}
