import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ConnectBDService } from './connect-bd.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import {of} from "rxjs";

describe('ConnectBDService', () => {

  let service: ConnectBDService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post']);
    service = new ConnectBDService(httpClientSpy);
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],providers: [ ConnectBDService ]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ConnectBDService);
  });



  /* CASO POR DEFECTO */
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /* 1 GET ----------------------- OK  */
  // Verifica si el método getClassificacio() realiza una solicitud GET correcta y devuelve la respuesta esperada.
  it('GET clasificación', () => {

    const res = {
      error: false,
      data: { },
      message: "Ranking"
    }
      spyOn( service, "getClassificacio").and.returnValue(of(res));


    service.getClassificacio().subscribe( response => {
      expect(res.message).toEqual(response.message);
    })


  });

  /* 2 POST  ----------------------- OK   */
  // Verifica si el método login() realiza una solicitud POST correcta y devuelve la respuesta esperada.
  it('login POST', () => {

    const form = new FormGroup({
      nom: new FormControl('username@ies-sabadell.cat'),
      password: new FormControl('Password_1234')
    });

    service.login(form).subscribe(response => {
      expect(response.message).toEqual('Select Ok.');
    });

    const req = httpTestingController.expectOne(service.REST_API + "/login");
    expect(req.request.method).toEqual('POST');

  });

  /* 3 POST  ------------------------ OK */
  // verifica si el método registerUserPacman() realiza una solicitud POST correcta y devuelve la respuesta esperada.
  it('register user Pacman', () => {
  /*forzamos esta respuesta */
    const res = {
      error: false,
      data: { },
      message: 'REGISTRO REALIZADO'
    }
    /* ponemos el espia */
    spyOn( service, "registerUserPacman").and.returnValue(of(res));

    /* preparamos lo que queremos testear */
    const pruebaForm = new FormGroup({
      id : new FormControl(19),
      nom : new FormControl('limoncin@ies-sabadell.cat'),
      password : new FormControl('Hora_de_4v3')
    });

    service.registerUserPacman(pruebaForm).subscribe(response => {
      expect(response.message).toEqual('REGISTRO REALIZADO');
    });
  });
});

